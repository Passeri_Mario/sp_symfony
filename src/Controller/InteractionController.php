<?php

namespace App\Controller;

use App\Entity\Interaction;
use App\Form\InteractionType;
use App\Repository\InteractionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/interaction")
 */
class InteractionController extends AbstractController
{
    /**
     * @Route("/", name="interaction_index", methods={"GET"})
     */
    public function index(InteractionRepository $interactionRepository): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('interaction/index.html.twig', [
                'interactions' => $interactionRepository->findAll(),
            ]);
        }else{
            return $this->redirectToRoute('app_login');
        }

    }

    /**
     * @Route("/new", name="interaction_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $interaction = new Interaction();
            $form = $this->createForm(InteractionType::class, $interaction);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($interaction);
                $entityManager->flush();

                return $this->redirectToRoute('interaction_index');
            }

            return $this->render('interaction/new.html.twig', [
                'interaction' => $interaction,
                'form' => $form->createView(),
            ]);
        }
        else{
            return $this->redirectToRoute('app_login');
        }

    }

    /**
     * @Route("/{id}", name="interaction_show", methods={"GET"})
     */
    public function show(Interaction $interaction): Response
    {
        return $this->render('interaction/show.html.twig', [
            'interaction' => $interaction,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="interaction_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Interaction $interaction): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $form = $this->createForm(InteractionType::class, $interaction);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('interaction_index');
            }

            return $this->render('interaction/edit.html.twig', [
                'interaction' => $interaction,
                'form' => $form->createView(),
            ]);
        }
        else{
            return $this->redirectToRoute('app_login');
        }
    }

    /**
     * @Route("/{id}", name="interaction_delete", methods={"POST"})
     */
    public function delete(Request $request, Interaction $interaction): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            if ($this->isCsrfTokenValid('delete'.$interaction->getId(), $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($interaction);
                $entityManager->flush();
            }

            return $this->redirectToRoute('interaction_index');
        }
        else{
            return $this->redirectToRoute('app_login');
        }
    }
}
