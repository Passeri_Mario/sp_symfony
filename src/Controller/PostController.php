<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\Subject;
use App\Form\PostType;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class PostController extends AbstractController
{
    /**
     * @Route("/", name="post_index", methods={"GET"})
     */
    public function index(PostRepository $postRepository): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('post/index.html.twig', [
                'posts' => $postRepository->findAll(),
            ]);
        }
        else{
            return $this->redirectToRoute('app_login');
        }
    }

    /**
     * @Route("/post/new/{id}", name="post_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser(); // récupère l'id de l'utilisateur connecté
            $subjectparam = $request->attributes->get('_route_params'); // récupère l'id de subject depuis l'URL
            $repository=$this->getDoctrine()->getRepository(Subject::class);
            $subject=$repository->find($subjectparam);
            $post = new Post();

            $post->setUserID($user);

            $post->setSubjectID($subject);

            $form = $this->createForm(PostType::class, $post);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($post);
                $entityManager->flush();

                return $this->redirectToRoute('home');
            }

            return $this->render('post/new.html.twig', [
                'post' => $post,
                'form' => $form->createView(),
            ]);
        }
        else{
            return $this->redirectToRoute('app_login');
        }

    }

    /**
     * @Route("/post/{id}", name="post_show", methods={"GET"})
     */
    public function show(Post $post): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('post/show.html.twig', [
                'post' => $post,
            ]);
        }
        else{
            return $this->redirectToRoute('subject');
        }
    }

    /**
     * @Route("/post/{id}/edit", name="post_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Post $post): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $form = $this->createForm(PostType::class, $post);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('post_index');
            }

            return $this->render('post/edit.html.twig', [
                'post' => $post,
                'form' => $form->createView(),
            ]);
        }
        else{
            return $this->redirectToRoute('app_login');
        }

    }

    /**
     * @Route("/post/{id}", name="post_delete", methods={"POST"})
     */
    public function delete(Request $request, Post $post): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            if ($this->isCsrfTokenValid('delete'.$post->getId(), $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($post);
                $entityManager->flush();
            }

            return $this->redirectToRoute('post_index');
        }
        else{
            return $this->redirectToRoute('app_login');
        }
    }
}
