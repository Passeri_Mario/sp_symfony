<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\SubjectRepository;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Entity\Subject;
use App\Form\SubjectType;

    class FrontController extends AbstractController
    {
        /**
         * @Route("/", name="home")
         */
        public function home(SubjectRepository $subjectRepository): Response
        {
            if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
                return $this->render('app/index.html.twig', [
                    'controller_name' => 'FrontController',
                    'subjects' => $subjectRepository->findAll()
                ]);
            }
            else{
                return $this->redirectToRoute('app_login');
            }
        }

        /**
         * @Route("/admin", name="admin")
         */
        public function admin(SubjectRepository $subjectRepository): Response
        {
            if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
                return $this->render('administration/admin.html.twig', [
                    'controller_name' => 'FrontController',
                    'subjects' => $subjectRepository->findAll()
                ]);
            }
            else{
                return $this->redirectToRoute('app_login');
            }
        }

        /**
         * @Route("admin/admin_subjects", name="admin_subjects")
         */
        public function adminSubjects(SubjectRepository $subjectRepository): Response
        {
            if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
                return $this->render('administration/admin_subjects.html.twig', [
                    'controller_name' => 'FrontController',
                    'subjects' => $subjectRepository->findAll()
                ]);
            }
            else{
                return $this->redirectToRoute('app_login');
            }
        }

        /**
         * @Route("admin/admin_users", name="admin_users")
         */
        public function adminUsers(UserRepository $userRepository): Response
        {
            if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
                return $this->render('administration/admin_users.html.twig', [
                    'controller_name' => 'FrontController',
                    'users' => $userRepository->findAll()
                ]);
            }
            else{
                return $this->redirectToRoute('app_login');
            }

        }

    }
