<?php

namespace App\Controller;

use App\Entity\Subject;

use App\Entity\User;
use App\Form\SubjectType;
use App\Repository\SubjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Repository\PostRepository;

class SubjectController extends AbstractController
{

    /**
     * @Route("/subject/new", name="subject_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            $subject = new Subject();
            $subject->setUserID($user);

            $form = $this->createForm(SubjectType::class, $subject);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($subject);
                $entityManager->flush();

                return $this->redirectToRoute('home');
            }

            return $this->render('subject/new.html.twig', [
                'subject' => $subject,
                'form' => $form->createView(),
            ]);
        }
        else{
            return $this->redirectToRoute('app_login');
        }
    }

    /**
     * @Route("/subject/{id}", name="subject_show", methods={"GET"})
     */
    public function show(Subject $subject, PostRepository $postRepository): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            return $this->render('subject/show.html.twig', [
                'subject' => $subject,
                'posts' => $postRepository->getPostSubject($subject)
            ]);
        }
        else{
            return $this->redirectToRoute('app_login');
        }
    }

    /**
     * @Route("/subject/edit/{id}", name="subject_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Subject $subject): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $form = $this->createForm(SubjectType::class, $subject);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('home');
            }

            return $this->render('subject/edit.html.twig', [
                'subject' => $subject,
                'form' => $form->createView(),
            ]);
        }
        else{
            return $this->redirectToRoute('app_login');
        }
    }

    /**
     * @Route("/subject/{id}", name="subject_delete", methods={"POST"})
     */
    public function delete(Request $request, Subject $subject): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            if ($this->isCsrfTokenValid('delete'.$subject->getId(), $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($subject);
                $entityManager->flush();
            }

            return $this->redirectToRoute('home');
        }
        else{
            return $this->redirectToRoute('app_login');
        }
    }
}
