<?php

namespace App\Entity;

use App\Repository\InteractionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InteractionRepository::class)
 */
class Interaction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Action::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $actionID;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="interactions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userID;

    /**
     * @ORM\ManyToOne(targetEntity=Subject::class, inversedBy="interactions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $subjectID;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getActionID(): ?Action
    {
        return $this->actionID;
    }

    public function setActionID(?Action $actionID): self
    {
        $this->actionID = $actionID;

        return $this;
    }

    public function getUserID(): ?User
    {
        return $this->userID;
    }

    public function setUserID(?User $userID): self
    {
        $this->userID = $userID;

        return $this;
    }

    public function getSubjectID(): ?Subject
    {
        return $this->subjectID;
    }

    public function setSubjectID(?Subject $subjectID): self
    {
        $this->subjectID = $subjectID;

        return $this;
    }
}
